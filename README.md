# GitLab CI template for Node.js

This project implements a generic GitLab CI template for projects based on [Node.js](https://nodejs.org/).

More precisely, it can be used by all projects based on [npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/) package managers.

It provides several features, usable in different modes (by configuration).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/node'
    ref: '3.0.0'
    file: '/templates/gitlab-ci-node.yml'
```

## Global configuration

The Node.js template uses some global configuration used throughout all jobs.

| Name                   | description                                                                                      | default value     |
|------------------------|--------------------------------------------------------------------------------------------------|-------------------|
| `NODE_IMAGE`           | The Docker image used to run Node.js <br/>:warning: **set the version required by your project** | `node:lts-alpine` |
| `NODE_MANAGER`         | The package manager used by your project (npm or yarn)<br/>**If undefined, automatic detection** | _none_            |
| `NODE_CONFIG_REGISTRY` | npm [registry](https://docs.npmjs.com/cli/v8/using-npm/registry)                                 | _none_            |
| `NODE_PROJECT_DIR`     | Node project root directory                                                                      | `.`               |
| `NODE_SOURCE_DIR`      | Sources directory                                                                                | `src`             |

## Jobs

### `node-lint` job

The Node template features a job `node-lint` that performs Node.js source code **lint**. This job is **disabled by default**. It can be activated by setting `NODE_LINT_ENABLED`

It is bound to the `test` stage, and uses the following variable:

| Name                     | description                                                                                                                                                                                                                | default value                 |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|
| `NODE_LINT_ENABLED`      | Set to `true` to enable lint analysis                                                                                                                                                                                      | _none_ (disabled)             |
| `NODE_LINT_ARGS`         | npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments to execute the lint analysis <br/> yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments to execute the lint analysis | `run lint`                    |

The job generates a lint report that you will find here: `NODE_PROJECT_DIR/reports/eslint-report.json`.

### `node-build` job

The Node template features a job `node-build` that performs **build and tests** all at once. You can disable the build using the variable **NODE_BUILD_DISABLED**

Those stages are performed in a single job for **optimization** purpose (it saves time) and also
for jobs dependency reasons (some jobs such as SONAR analysis have a dependency on test results).

This job is bound to the `build` stage, and uses the following variables:

| Name                          | description                                                                                                                                                       | default value         |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|
| `NODE_BUILD_DISABLED`         | Set to `true` to disable build                                                                                                                                    | _none_ (enabled)      |
| `NODE_BUILD_DIR`              | Variable to define build directory                                                                                                                                | `dist`                |
| `NODE_BUILD_ARGS`             | npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments <br/> yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments  | `run build --prod`    |
| `NODE_TEST_ARGS`              | npm [test](https://docs.npmjs.com/cli/v8/commands/npm-test) arguments <br/> yarn [test](https://classic.yarnpkg.com/en/docs/cli/test) arguments                   | `test -- --coverage`  |

The job generates a unit test report that you will find here: `NODE_PROJECT_DIR/reports/unit_test_report.xml`.

#### Unit testing with Jest

If you're using [Jest](https://jestjs.io/) as unit testing framework, you'll have to make the following configuration in
order to [integrate your unit tests results to GitLab](https://docs.gitlab.com/ee/ci/unit_test_reports.html) (and optionally to SonarQube).

##### Coverage

For coverage, Jest comes built-in with **Istanbul** package. So no need for extra dependency.

```js
"jest": {
...
    "coverageDirectory": './reports',
...
},
```

##### Junit Report

By default Jest doesn't generate any test report supported by GitLab. To do so you need to use the [jest-junit](https://www.npmjs.com/package/jest-junit) package.

Add the package as a development dependency:

```bash
npm install --save-dev jest-junit
```

Then update your `jest.config.js` or `package.json` as follows:

```js
"jest": {
...
    "reporters": [
        "default",
        [
            "jest-junit",
            {
                "outputDirectory": "reports",
                "outputName": "unit_test_report.xml"
            }
        ]
    ]
...
},
```

##### Sonar report

By default Jest doesn't generate any test report supported by Sonar. To do so you need to use the [jest-sonar](https://www.npmjs.com/package/jest-sonar) package.

Add the package as a development dependency:

```bash
npm install --save-dev jest-sonar
```

Then update your `jest.config.js` or `package.json` as follows:

```js
"jest": {
...
    "reporters": [
        "default",
        [
            "jest-junit",
            {
                "outputDirectory": "reports",
                "outputName": "unit_test_report.xml"
            }
        ],
        [
            "jest-sonar",
            {
                "outputDirectory": "reports",
                "outputName": "sonar_test_report.xml"
            }
        ]
    ],
...
},
```

#### Unit testing with Mocha

If you're using [Mocha](https://mochajs.org/) as unit testing framework, you'll have to make the following configuration in
order to [integrate your unit tests results to GitLab](https://docs.gitlab.com/ee/ci/unit_test_reports.html) (and optionally to SonarQube).

##### Coverage

By default Mocha doesn't provide coverage. To do so you need to use the [Istanbul](https://www.npmjs.com/package/nyc) package.

Add the package as a development dependency:

```shell
npm install --save-dev nyc
```

Then update your `package.json` as follows:

```js
"scripts": {
  "test": "npm run mocha",
  "mocha": "nyc --reporter=lcov --report-dir=./reports --reporter=text mocha app/tests/*.test.js",
}
```

##### Junit report

By default Mocha doesn't generate any test report supported by GitLab. To do so you need to use the [mocha-junit-reporter](https://www.npmjs.com/package/mocha-junit-reporter) package.

Add the package as a development dependency:

```shell
npm install --save-dev mocha-junit-reporter
```

Then update your `package.json` as follows:

```js
"scripts": {
  "test": "npm run mocha",
  "mocha": "nyc mocha app/tests/*.test.js --reporter spec --reporter mocha-junit-reporter --reporter-options mochaFile=./reports/unit_test_report.xml",
}
```

##### Sonar report

Mocha provides XUnit reporter which is compatible with SonarQube. So no need for extra dependency, just add this parameter `--reporter-option`

Then update your `package.json` as follows:

```js
"scripts": {
  "test": "npm run mocha",
  "mocha": "nyc mocha app/tests/*.test.js --reporter-option output=./reports/sonar_test_report.xml",
}
```

#### Unit testing with Jasmine

Support of [Jasmine](https://jasmine.github.io/) as unit testing framework is not documented yet and will come soon in a further
version of this template.

### SonarQube analysis

If you're using the SonarQube template to analyse your Node code, here are 2 sample `sonar-project.properties` files.

If using JavaScript language:

```properties
# see: https://docs.sonarqube.org/latest/analysis/languages/javascript/
# set your source directory(ies) here (relative to the sonar-project.properties file)
sonar.sources=.
# exclude unwanted directories and files from being analysed
sonar.exclusions=node_modules/**,dist/**,**/*.test.js

# set your tests directory(ies) here (relative to the sonar-project.properties file)
sonar.tests=.
sonar.test.inclusions=**/*.test.js

# tests report: generic format
sonar.testExecutionReportPaths=reports/sonar_test_report.xml
# lint report: ESLint JSON
sonar.eslint.reportPaths=reports/eslint-report.json
# coverage report: LCOV format
sonar.javascript.lcov.reportPaths=reports/lcov.info
```

If using TypeScript language:

```properties
# see: https://docs.sonarqube.org/latest/analysis/languages/typescript/
# set your source directory(ies) here (relative to the sonar-project.properties file)
sonar.sources=src
# exclude unwanted directories and files from being analysed
sonar.exclusions=node_modules/**,dist/**,**/*.spec.ts

# set your tests directory(ies) here (relative to the sonar-project.properties file)
sonar.tests=src
sonar.test.inclusions=**/*.spec.ts

# tests report: generic format
sonar.testExecutionReportPaths=reports/sonar_test_report.xml
# lint report: TSLint JSON
sonar.typescript.tslint.reportPaths=reports/tslint-report.json
# coverage report: LCOV format
sonar.typescript.lcov.reportPaths=reports/lcov.info
```

More info:

* [JavaScript language support](https://docs.sonarqube.org/latest/analysis/languages/javascript/)
* [TypeScript language support](https://docs.sonarqube.org/latest/analysis/languages/typescript/)
* [test coverage & execution parameters](https://docs.sonarqube.org/latest/analysis/coverage/)
* [third-party issues](https://docs.sonarqube.org/latest/analysis/external-issues/)

### `node-audit` job

The Node template features a job `node-audit` that performs an audit ([npm audit](https://docs.npmjs.com/cli/v8/commands/npm-audit) or [yarn audit](https://classic.yarnpkg.com/en/docs/cli/audit)) to find vulnerabilities (security).

It is bound to the `test` stage.


| Name                   | description                                                                                                                                           | default value                    |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------|
| `NODE_AUDIT_DISABLED`  | Set to `true` to disable npm audit                                                                                                                    | _none_ (enabled)                 |
| `NODE_AUDIT_ARGS`      | npm [audit](https://docs.npmjs.com/cli/v8/commands/npm-audit) arguments <br/> yarn [audit](https://classic.yarnpkg.com/en/docs/cli/audit) arguments   | `--audit-level=low`              |

The job generates an audit report that you will find here: `NODE_PROJECT_DIR/reports/npm-audit-report.json`.

### `node-npm-outdated` job

The Node template features a job `node-outdated` that performs outdated analysis ([npm outdated](https://docs.npmjs.com/cli/v8/commands/npm-outdated) or [yarn outdated](https://classic.yarnpkg.com/lang/en/docs/cli/outdated/)) to find dependencies that might be updated.

It is bound to the `test` stage.

| Name                      | description                                                                                                                                                           | default value                      |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| `NODE_OUTDATED_DISABLED`  | Set to `true` to disable npm outdated                                                                                                                                 | _none_ (enabled)                   |
| `NODE_OUTDATED_ARGS`      | npm [outdated](https://docs.npmjs.com/cli/v8/commands/npm-outdated) arguments <br/> yarn [outdated](https://classic.yarnpkg.com/lang/en/docs/cli/outdated/) arguments | `--long`                           |

The job generates an outdated report that you will find here: `NODE_PROJECT_DIR/reports/npm-outdated-report.json`.
