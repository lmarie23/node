# [3.0.0](https://gitlab.com/to-be-continuous/node/compare/2.4.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([ed3af02](https://gitlab.com/to-be-continuous/node/commit/ed3af028e090fa6deb8dd20ca64039a8f14bacd3))
* switch to MR pipeline as default ([4e66d5d](https://gitlab.com/to-be-continuous/node/commit/4e66d5d5d0240792bd86d1ea705ec60f09a2c553))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.4.0](https://gitlab.com/to-be-continuous/node/compare/2.3.0...2.4.0) (2022-08-02)


### Bug Fixes

* **cache:** Using variable $NODE_MANAGER won't work if auto-detection is enabled (cache path is evaluated before executing our scripts) ([b54c72a](https://gitlab.com/to-be-continuous/node/commit/b54c72a6e3e7c45d9f5e807b4c44f32d32834980))
* **config_registry:** rename var ([e43446a](https://gitlab.com/to-be-continuous/node/commit/e43446a39694ce41ca45f9a6ac529403d788aba0))
* hardcode all variabilized report variables ([ab797c2](https://gitlab.com/to-be-continuous/node/commit/ab797c255b18cd2ec0f0281a1ca676ed754daa3d))
* **jest:** junit_test_report as file name instead of unit_test_report ([6d63868](https://gitlab.com/to-be-continuous/node/commit/6d6386889fe06fe66a80ca64281b46a6b33cedac))
* **jest:** revert junit_test_report as file name ([1050908](https://gitlab.com/to-be-continuous/node/commit/1050908858f6112289c60430ceda954c845b07f6))
* **node-audit:** generate json report only on job failure ([7270fd3](https://gitlab.com/to-be-continuous/node/commit/7270fd3ee26a6f5233890ede00ca74eb3ebdd3af))
* **node-build:** remove --bail option from NODE_TEST_ARGS default value ([584e880](https://gitlab.com/to-be-continuous/node/commit/584e880123c86d9263d7c5edf1afa2447c680ae2))
* **node-outdated:** generate json report only on job failure ([481783c](https://gitlab.com/to-be-continuous/node/commit/481783c2bb1a4d37ac4e1be59010189184e41fe3))
* **node-outdated:** NODE_OUTDATED_DISABLED should be a boolean ([822c7a6](https://gitlab.com/to-be-continuous/node/commit/822c7a64d859fc65785a1a5d06707c0dc088ab36))
* **node-outdated:** remove silent parameter (bad practice) ([46b049b](https://gitlab.com/to-be-continuous/node/commit/46b049be8d75f3171c07936b521ee9d66c0ecc8b))


### Features

* multiple node manager support (npm and yarn) ([d1ab6f8](https://gitlab.com/to-be-continuous/node/commit/d1ab6f83394c82130b93326a411362b39e069a38))
* remove html reports ([937671d](https://gitlab.com/to-be-continuous/node/commit/937671d2da66a35251634b0651da05ce2ccf4ddc))
* remove node-js-scan job ([48f73af](https://gitlab.com/to-be-continuous/node/commit/48f73afbcd6398431393f19e0e8fca9b84a8a91a))
* silent mode for node-outdated job ([8125cfa](https://gitlab.com/to-be-continuous/node/commit/8125cfa863164c41d94e84d51faa7601ac58ab69))

# [2.3.0](https://gitlab.com/to-be-continuous/node/compare/2.2.0...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([545d836](https://gitlab.com/to-be-continuous/node/commit/545d8361de9cecfa6dabd0f77ea023980b50d207))

# [2.2.0](https://gitlab.com/to-be-continuous/node/compare/2.1.0...2.2.0) (2022-01-04)


### Features

* **npmoutdated:** add npm outdated job ([3c6c261](https://gitlab.com/to-be-continuous/node/commit/3c6c2610d994ed822a0331629570f025279a7d2a))

# [2.1.0](https://gitlab.com/to-be-continuous/node/compare/2.0.2...2.1.0) (2021-11-23)


### Features

* **nodejsscan:** add SARIF output format ([b95ca31](https://gitlab.com/to-be-continuous/node/commit/b95ca3108a05a892313d8b6bfd4068404b73742c)), closes [#9](https://gitlab.com/to-be-continuous/node/issues/9)

## [2.0.2](https://gitlab.com/to-be-continuous/node/compare/2.0.1...2.0.2) (2021-10-13)


### Bug Fixes

* eslint report JSON format ([64432f2](https://gitlab.com/to-be-continuous/node/commit/64432f24813341e680b5f7af74f08076d30ddede))

## [2.0.1](https://gitlab.com/to-be-continuous/node/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([f3285e7](https://gitlab.com/to-be-continuous/node/commit/f3285e7f452e8d1ce92f38a3bc0bbdc891983d4c))

## [2.0.0](https://gitlab.com/to-be-continuous/node/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([6d4ba9d](https://gitlab.com/to-be-continuous/node/commit/6d4ba9ddc6a4cd1e670ab6dcb21f1e832894255e))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/node/compare/1.1.1...1.2.0) (2021-06-11)

### Features

* move group ([4d3219b](https://gitlab.com/to-be-continuous/node/commit/4d3219b87eb2e8eb2b09cf560bce09abb28aa027))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.1.0...1.1.1) (2021-05-19)

### Bug Fixes

* **lint:** remove useless --force param ([347faa8](https://gitlab.com/Orange-OpenSource/tbc/node/commit/347faa83d11a1d613d1bd479ec103cea62db89ec))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([bc25fa0](https://gitlab.com/Orange-OpenSource/tbc/node/commit/bc25fa02e8052973b932273605ece936718f3fe3))

## 1.0.0 (2021-05-06)

### Features

* initial release ([07fdee7](https://gitlab.com/Orange-OpenSource/tbc/node/commit/07fdee71d89f3f3413a64cf6151d0bb4a0daa971))
